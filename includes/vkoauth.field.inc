<?php

/**
 * Add field info to a Drupal user array (before account creation).
 */
function vkoauth_field_create_user(&$edit, $vkuser) {
  $field_map = variable_get('vkoauth_user_mapping', array());
  $field_convert_info = vkoauth_field_convert_info();
  $instances = field_info_instances('user', 'user');
  foreach ($instances as $field_name => $instance) {
    $field = field_info_field($instance['field_name']);
    if (isset($field_map[$field_name]) && isset($field_convert_info[$field['type']]['callback'])) {
      $callback = $field_convert_info[$field['type']]['callback'];
      $property_name = $field_map[$field_name];
      
      if ($value = $callback($property_name, $vkuser, $field, $instance)) {
        $edit[$field_name][LANGUAGE_NONE][0] = $value;
      }
    }
  }
}
/**
 * Provide a callback map for converting VK data to fields.
 */
function vkoauth_field_convert_info() {
  $convert_info = array(
    'text' => array(
      'label' => t('Text'),
      'callback' => 'vkoauth_field_convert_text',
    ),
    'text_long' => array(
      'label' => t('Long text'),
      'callback' => 'vkoauth_field_convert_text',
    ),
    'list_text' => array(
      'label' => t('List (\'text\')'),
      'callback' => 'vkoauth_field_convert_list',
    ),
    'datetime' => array(
      'label' => t('Date'),
      'callback' => 'vkoauth_field_convert_date',
    ),
    'date' => array(
      'label' => t('Date'),
      'callback' => 'vkoauth_field_convert_date',
    ),
    'datestamp' => array(
      'label' => t('Date'),
      'callback' => 'vkoauth_field_convert_date',
    ),
  );
  drupal_alter('vkoauth_field_convert_info', $convert_info);
  return $convert_info;
}

/**
 * VK data conversion function.
 */
function vkoauth_field_convert_text($property_name, $vkuser, $field, $instance) {
  $value = NULL;
  if (isset($vkuser[$property_name])) {
    if (is_string($vkuser[$property_name])) {
      $value = $vkuser[$property_name];
    }
  }
  return $value ? array('value' => $value) : NULL;
}

/**
 * VK data conversion function.
 */
function vkoauth_field_convert_list($property_name, $vkuser, $field, $instance) {

  if (!is_string($vkuser[$property_name])) {
    return;
  }

  $options = list_allowed_values($field);
  $best_match = 0.0;
  $best_option = NULL;
  $vk_option = is_string($vkuser[$property_name]) ? $vkuser[$property_name] : '';
  $match_vk = strtolower($vkuser[$property_name]);
  foreach ($options as $key => $option) {
    $option = trim($option);
    $match_option = strtolower($option);
    $this_match = 0;
    similar_text($match_option, $match_vk, $this_match);
    if ($this_match > $best_match) {
      $best_match = $this_match;
      $best_option = $option;
      $best_key = $key;
    }
  }

  return isset($best_key) ? array('value' => $best_key) : NULL;
}

/**
 * VK data conversion function.
 */
function vkoauth_field_convert_date($property_name, $vkuser, $field, $instance) {

  $value = NULL;
  $vkdate = explode('/', $vkuser[$property_name]);
  if (count($vkdate) == 3) {
    $date = new DateObject($vkuser[$property_name]);
    if (date_is_date($date)) {
      $format = $field['type'] == 'datestamp' ? DATE_FORMAT_UNIX : DATE_FORMAT_ISO;
      $value = array(
        'value' => $date->format($format, TRUE),
        'date_type' => $field['type'],
      );
    }
  }
  return $value;
}
<?php
/**
 * Provided themed information about the user's current VK connection.
 */
function theme_vkoauth_user_info($variables) {
  $account = $variables['account'];
  $permissions = array(0 => t('Basic information: First name, Last Name, ID, Gender, Picture'));
  $user_permissions = _vkoauth_user_permissions();
  if ($extra_permissions = variable_get('vkoauth_permissions', array())) {
    foreach ($extra_permissions as $key => $value) {
      if (isset($user_permissions[$key])) {
        $permissions[] = $user_permissions[$key];
      }
    }
  }
  $vkid = vkoauth_vkid_load($account->uid);
  $output = '';
  if ($vkid !== FALSE) {
    $output .= '<p>' . t('Your account is current connected with VK. Currently this site has access to the following information from VK.com.') . '</p>';
    $output .= theme('item_list', array('items' => $permissions));
    if (user_access('vkoauth deauthorize')) {
      $output .= '<p>' . t('You may disconnect your account from VK at any time by using the Deauthorize option below. If deauthorized, you will no longer be able to use VK.com to log into this account and must use a normal password. <strong>If you have not yet set a password, you will not be able to log onto this site</strong>.') . '</p>';
      $output .= vkoauth_action_display('deauth');
    }
  }
  else {
    $output .= '<p>' . t('Your account is not currently connected with VK. To connect this account to VK, click on the link below.') . '</p>';
    $output .= vkoauth_action_display('connect');
  }
  return $output;
}
/**
 * Display the VK Connect options on a user's profile form.
 */
function theme_vkoauth_user_form_connect($variables) {
  $uid = $variables['uid'];
  $vkid = $variables['vkid'];
  if ($vkid) {
    $output = t('Your account is connected with Vkontakte. (<a href="!url">More info</a>)', array('!url' => url('user/' . $uid . '/vkoauth')));
  }
  else {
    $output = vkoauth_action_display('connect');
    $output .= '<div class="description">' . t('Connect with VK to login with your VK account instead of a password.') . '</div>';
  }
  return $output;
}
/**
 * Return a link to initiate a VK Connect login or association.
 */
function theme_vkoauth_action__connect($variables) {
  $action = $variables['action'];
  $link = $variables['properties'];
  $url = url($link['href'], array('query' => $link['query']));
  $link['attributes']['class'] = isset($link['attributes']['class']) ? $link['attributes']['class'] : 'vk-action-connect';
  $attributes = isset($link['attributes']) ? drupal_attributes($link['attributes']) : '';
  $title = isset($link['title']) ? check_plain($link['title']) : '';
  $src = base_path() . drupal_get_path('module', 'vkoauth') . '/images/vk.png';
  return '<a ' . $attributes . ' href="' . $url . '"><img src="' . $src . '" alt="' . $title . '" /></a>';
}
/**
 * Return a button to perform different actions.
 */
function theme_vkoauth_action($variables) {
  $action = $variables['action'];
  $link = $variables['properties'];
  $link['attributes']['class'] = isset($link['attributes']['class']) ? $link['attributes']['class'] : 'form-button vk-button vk-action-' . str_replace('_', '-', $action['name']);
  $link['attributes']['name'] = isset($link['attributes']['name']) ? $link['attributes']['name'] : 'vk_action_' . $action['name'];
  $link['attributes']['type'] = 'button';
  $attributes = drupal_attributes($link['attributes']);
  $url = url($link['href'], array('query' => $link['query']));
  $content = '<button ' . $attributes . ' onclick="window.location = \'' . $url . '\'; return false;">' . check_plain($action['title']) . '</button>';
  return $content;
}
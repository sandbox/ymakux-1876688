<?php

/**
 * Display the settings form for VkOAuth.
 */
function vkoauth_settings_form($form, &$form_state) {

  $form['#tree'] = TRUE;

  $form['id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#size' => 20,
    '#maxlengh' => 50,
    '#description' => t('To use VK connect, a VK Application must be created. <a href="http://vk.com/editapp?act=create" target="_blank">Create app</a> on VK.com.') . ' ' . t('Enter your App ID here.'),
    '#default_value' => variable_get('vkoauth_id', ''),
  );
  $form['secret'] = array(
    '#type' => 'textfield',
    '#title' => t('App Secret'),
    '#size' => 40,
    '#maxlengh' => 50,
    '#description' => t('To use VK connect, a VK Application must be created. <a href="http://vk.com/editapp?act=create" target="_blank">Create app</a> on VK.com.') . ' ' . t('Enter your App Secret here.'),
    '#default_value' => variable_get('vkoauth_secret', ''),
  );
  
  $form['require_email'] = array(
    '#title' => t('Require email'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('vkoauth_require_email', TRUE),
    '#description' => t('VK does not provide user emails. If selected, an additional form will be shown to force users to specify their email addresses.'),
  );
  
  $description = t('Select the VK value used to set the user\'s name when connecting with VK.com.');
  $description .= t('Variables: <em>%vkid</em> - numeric user ID, <em>%first_name</em> - user\'s first name, <em>%last_name</em> - user\'s last name');
  
  $form['username'] = array(
    '#title' => t('Username pattern'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('vkoauth_username', '%first_name %last_name'),
    '#description' => $description,
  );
  
  if (module_exists('transliteration')) {
    $form['translit'] = array(
      '#title' => t('Transliterate'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('vkoauth_translit', FALSE),
      '#description' => t('Transliterate username'),
    );
  }
  
  $languages = array();
  foreach (language_list() as $language) {
    $languages[$language->language] = check_plain($language->native);
  }
  
  $form['language'] = array(
    '#title' => t('Language'),
    '#type' => 'select',
    '#options' => $languages,
    '#default_value' => variable_get('vkoauth_language', language_default('language')),
    '#description' => t('Set this user language when connected with VK.com'),
  );
  
  $form['login_form'] = array(
    '#title' => t('Show on login form'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('vkoauth_login_form', 1),
  );
  
  $form['login_form_weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight',
    '#default_value' => variable_get('vkoauth_login_form_weight', 10),
    '#description' => t('Position of VK connect button in login form'),
    '#states' => array(
      'visible' => array(
        ':input[name="login_form"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  $roles = array_map('check_plain', user_roles(TRUE));
  $form['roles_list'] = array(
    '#type' => 'value',
    '#value' => $roles,
  );
  
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'), 
    '#default_value' => variable_get('vkoauth_roles', array()), 
    '#options' => $roles,
    '#description' => t('Set these roles when user profile created during VK.com connection'),
  );
  
  $form['mapping'] = array(
    '#title' => t('Mapping'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Each of your <a href="@url">fields that are attached to users</a> are listed below. Map each one you would like to import into your site to a VK data source.', array('@url' => url('admin/config/people/accounts/fields'))),
  );
  
  $form['permissions'] = array(
    '#title' => t('Permissions'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('If handling mapping manually or integrating with a custom module, you may request additional permissions for your application here. Requesting more information from users may make them less likely to trust your website. Note that enabling more permissions here does not import more data. It just makes this information available for you to access in a custom module.'),
  );
  
  $form['permissions']['permissions'] = array(
    '#title' => t('Permissions'),
    '#type' => 'checkboxes',
    '#options' => _vkoauth_user_permissions(),
    '#default_value' => variable_get('vkoauth_permissions', array()),
  );
  
  $properties = vkoauth_user_fields_mapping();
  $property_options = array();
  foreach ($properties as $property => $property_info) {
    if (isset($property_info['field_types'])) {
      foreach ($property_info['field_types'] as $field_type) {
        $property_options[$field_type][$property] = '[' . $property . '] ' . $property_info['label'];
      }
    }
  }
  
  $field_defaults = variable_get('vkoauth_user_mapping', array());
  $instances = field_info_instances('user', 'user');
  foreach ($instances as $field_name => $instance) {
    $field = field_info_field($instance['field_name']);
    if (isset($property_options[$field['type']])) {
      $options = array_merge(array('' => t('- Do not import -')), $property_options[$field['type']]);
      $form['mapping'][$field_name] = array(
        '#title' => t($instance['label']),
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => isset($field_defaults[$field_name]) ? $field_defaults[$field_name] : '',
      );
    }
    else {
      $form['mapping'][$field_name] = array(
        '#title' => t($instance['label']),
        '#type' => 'form_element',
        '#children' => '<em>' . t('No mappable VK properties.') . '</em>',
        '#theme_wrappers' => array('form_element'),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Form validation function for vkoauth_settings_form().
 */
function vkoauth_settings_form_validate($form, &$form_state) {
  // Remove trailing spaces from keys.
  $form_state['values']['id'] = trim($form_state['values']['id']);
  $form_state['values']['secret'] = trim($form_state['values']['secret']);
  $form_state['values']['username'] = trim($form_state['values']['username']);

  $form_state['values']['mapping'] = array_filter($form_state['values']['mapping']);
  $form_state['values']['permissions']['permissions'] = array_filter($form_state['values']['permissions']['permissions']);

  // Check if App Id has numeric value.
  if (!is_numeric($form_state['values']['id'])) {
    form_error($form['id'], t('The App ID must be an integer.'));
  }
  
  $roles = array();
  foreach ($form_state['values']['roles_list'] as $key => $value) {
    if (!empty($form_state['values']['roles'][$key])) {
      $roles[$key] = $value;
    }
  }
  
  $form_state['values']['roles'] = $roles;
}


/**
 * Form submission function for vkoauth_settings_form().
 */
function vkoauth_settings_form_submit($form, &$form_state) {
  
  // Save submitted values. TODO: use foreach
  variable_set('vkoauth_id', $form_state['values']['id']);
  variable_set('vkoauth_secret', $form_state['values']['secret']);
  variable_set('vkoauth_require_email', $form_state['values']['require_email']);
  variable_set('vkoauth_user_mapping', $form_state['values']['mapping']);
  variable_set('vkoauth_permissions', $form_state['values']['permissions']['permissions']);
  variable_set('vkoauth_translit', $form_state['values']['translit']);
  variable_set('vkoauth_username', $form_state['values']['username']);
  variable_set('vkoauth_roles', $form_state['values']['roles']);
  variable_set('vkoauth_language', $form_state['values']['language']);
  variable_set('vkoauth_login_form', $form_state['values']['login_form']);
  variable_set('vkoauth_login_form_weight', $form_state['values']['login_form_weight']);

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Display the email form that allows users to provide their e-mails.
 */
function vkoauth_set_email_form($form, &$form_state) {

  $form['text'] = array(
    '#markup' => t('We can\'t access your VK e-mail. In order to have account on this site, you must provide an email address. Please specify email that will be associated with your account on this site.'),
  );

  $form['email'] = array(
    '#title' => t('E-mail'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Specify email to be used for your account on this site'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Form validation function for vkoauth_set_email_form().
 */
function vkoauth_set_email_form_validate($form, &$form_state) {

  $form_state['values']['email'] = trim($form_state['values']['email']);
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('You must enter a valid e-mail address.'));
  }
}

/**
 * Form submission function for vkoauth_set_email_form_submit().
 */
function vkoauth_set_email_form_submit($form, &$form_state) {

  $params = $_SESSION['vkoauth'];
  $destination = !empty($params['destination']) ? $params['destination'] : '<front>';
  $vkid = $params['uid'];

  $params['email'] = $form_state['values']['email'];
  vkoauth_create_login_user($params);

  unset($_SESSION['vkoauth']);
  $form_state['redirect'] = $destination;
}
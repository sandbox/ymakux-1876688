<?php

/* ============================== CONSTANS ============================== */

define('VKOAUTH_ACCESS_TOKEN_URL', 'https://api.vkontakte.ru/oauth/access_token');
define('VKOAUTH_AUTHORIZE_URL', 'http://api.vkontakte.ru/oauth/authorize');
define('VKOAUTH_DEAUTHORIZE_URL', 'https://oauth.vk.com/logout');
define('VKOAUTH_METHOD_URL', 'https://api.vk.com/method');
define('VKOAUTH_EMAIL_DOMAIN', 'vkontakte.ru');
define('VKOAUTH_AVATAR_SIZE', 'photo_medium');

/* ============================== HOOKS ============================== */

/**
 * Implements hook_permission().
 */
function vkoauth_permission() {
  return array( 
    'vkoauth connect' => array(
      'title' => t('Connect'), 
      'description' => t('Connect with VK.com'),
    ),
    'vkoauth create account' => array(
      'title' => t('Create account'), 
      'description' => t('Create account on this site upon VK connect'),
    ),
    'vkoauth deauthorize' => array(
      'title' => t('Disconnect'), 
      'description' => t('Break connection between this site and VK.com'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function vkoauth_menu() {

  $items['vkoauth/%vkoauth_action'] = array(
    'page callback' => 'vkoauth_action_page',
    'page arguments' => array(1),
    'access arguments' => array('vkoauth connect'),
    'type' => MENU_CALLBACK,
  );

  $items['vkoauth/deauthorize'] = array(
    'page callback' => 'vkoauth_deauthorize',
    'access arguments' => array('vkoauth deauthorize'),
    'type' => MENU_CALLBACK,
  );

  $items['vkoauth/email'] = array(
    'title' => 'E-mail',
    'description' => t('Enter E-mail for account'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vkoauth_set_email_form'),
    'file' => 'includes/vkoauth.admin.inc',
    'access arguments' => array('vkoauth create account'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/config/people/vkoauth'] = array(
    'title' => t('Vkontakte OAuth'),
    'description' => 'Configure site for Vkontakte Connect and map VK\'s information to user profiles.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vkoauth_settings_form'),
    'access arguments' => array('administer users'),
    'file' => 'includes/vkoauth.admin.inc',
  );

  $items['user/%user/vkoauth'] = array(
    'title' => t('Vkontakte settings'),
    'description' => 'User information about current VK connection',
    'page callback' => 'vkoauth_user_info',
    'page arguments' => array(1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function vkoauth_theme() {
  return array(
    
    'vkoauth_action' => array(
      'pattern' => 'vkoauth_action__[a-z0-9_]+',
      'variables' => array('action' => NULL, 'properties' => NULL),
      'file' => 'includes/vkoauth.theme.inc',
    ),

    'vkoauth_action__connect' => array(
      'variables' => array('action' => NULL, 'properties' => NULL),
      'file' => 'includes/vkoauth.theme.inc',
    ),

    'vkoauth_user_form_connect' => array(
      'variables' => array('uid' => NULL, 'vkid' => NULL),
      'file' => 'includes/vkoauth.theme.inc',
    ),
    
    'vkoauth_user_info' => array(
      'variables' => array('account' => NULL),
      'file' => 'includes/vkoauth.theme.inc',
    ),
  );
}

/**
 * Implements hook_user_delete().
 */
function vkoauth_user_delete($account) {
  vkoauth_save($account->uid);
}

/**
 * Implements hook_block_info().
 */
function vkoauth_block_info() {
  $blocks['login'] = array(
    'info' => t('VK login'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function vkoauth_block_view($delta) {
  $block = array();
  if (user_access('vkoauth connect') && variable_get('vkoauth_id', '') && !vkoauth_vkid_load()) {
    $block['content'] = vkoauth_action_display('connect');
  }
  return $block;
}

/**
 * Implements hook_form_alter().
 */
function vkoauth_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'user_login_block' || $form_id == 'user_login') {
    $i = 0;
    foreach (element_children($form) as $child) {
      $form[$child]['#weight'] = $i;
      $i++;
    }
    
    if (variable_get('vkoauth_login_form', 1) && user_access('vkoauth connect')) {
      $form['vkoauth'] = array(
        '#markup' => vkoauth_action_display('connect'),
        '#weight' => variable_get('vkoauth_login_form_weight'),
      );
    }
  }
  
  if ($form_id == 'user_profile_form') {
    $uid = $form['#user']->uid;
    $vkid = vkoauth_vkid_load($uid);

    $vkoauth_form = array(
      '#type' => 'item',
      '#title' => t('Vkontakte connect'),
      '#markup' => theme('vkoauth_user_form_connect', array('uid' => $uid, 'vkid' => $vkid)),
    );

    $account_form = isset($form['account']) ? $form['account'] : $form;

    // If a user has created/linked an account through a VK login, remove
    // the current password field until they set a password.
    if ($vkid && $form['#user']->pass == '') {
      if (empty($form_state['input'])) {
        drupal_set_message(t('Please set a password to secure your account details.'), 'warning', FALSE);
      }
      unset($account_form['current_pass']);
      $account_form['current_pass_required_values']['#value'] = array();
    }

    // Inject the VK options after the e-mail settings. No weights are on
    // these elements by default, so we have to put it in order.
    $temp_form = array();
    foreach (element_children($account_form) as $child) {
      $temp_form[$child] = $account_form[$child];
      if ($child == 'mail') {
        if (isset($temp_form[$child]['#weight'])) {
          $vkoauth_form['#weight'] = $temp_form[$child]['#weight'];
        }
        $temp_form['vkoauth'] = $vkoauth_form;
      }
      unset($account_form[$child]);
    }

    $account_form += $temp_form;
    if (isset($form['account'])) {
      $form['account'] = $account_form;
    }
    else {
      $form = $account_form;
    }
  }
}

/**
 * Implements hook_vkoauth_actions().
 */
function vkoauth_vkoauth_actions() {
  
  global $user;
  
  $actions = array();
  $actions['connect'] = array(
    'title' => t('Connect'),
    'callback' => 'vkoauth_action_connect',
    'permissions' => array_keys(variable_get('vkoauth_permissions', array())),
  );

  $actions['deauth'] = array(
    'title' => t('Deauthorize'),
    'callback' => 'vkoauth_action_deauth',
    'properties' => array('href' => 'vkoauth/deauthorize', 'query' => array(
      'uid' => $user->uid, 'destination' => 'user/' . $user->uid . '/edit')),
  );

  return $actions;
}

/* ============================== MENU CALLBACKS ============================== */

/**
 * Menu callback; Information about current VK connection.
 */
function vkoauth_user_info($account) {
  return theme('vkoauth_user_info', array('account' => $account));
}

/**
 * Menu callback; Logout VK application.
 */
function vkoauth_deauthorize() {
  vkoauth_action_deauth();
}

/**
 * Menu callback; The main page for processing VK login transactions.
 */
function vkoauth_action_page($action) {
  global $user;
  $app_id = variable_get('vkoauth_id', '');
  $app_secret = variable_get('vkoauth_secret', '');
  $action_name = $action['name'];
  $error_message = t('The VK login could not be completed due to an error. Please create an account or contact us directly. Details about this error have already been recorded to the error log.');
  
  if (!($app_id && $app_secret)) {
    watchdog('vkoauth', 'A VK login was attempted but could not be processed because the module is not yet configured. Vist the <a href="!url">VK OAuth configuration</a> to set up the module.', array('!url' => url('admin/config/people/vkoauth')));
  }
  elseif (isset($_REQUEST['error'])) {
    $link = vkoauth_action_link_properties($action_name, $app_id);
    watchdog('vkoauth', 'A user refused to allow access to the necessary VK information (@permissions) to login to the site.', array('@permissions' => $link['query']['scope']));
    $error_message = t('This site requires access to information in order to log you into the site. If you like you can <a href="!vk">sign in with VK again</a> or <a href="!register">register normally</a>.', array(
      '!vk' => url($link['href'], array(
        'query' => $link['query'])),
        '!register' => url('user/register')));
  }
  elseif (!isset($_REQUEST['code'])) {
    watchdog('vkoauth', 'A VK request code was expected but no authorization was received.');
  }
  elseif ($access_token = vkoauth_access_token($_REQUEST['code'], $action_name, $app_id, $app_secret)) {
    $action = vkoauth_action_invoke($action_name, $app_id, $access_token);
    if (!empty($action['destination'])) {
      $destination = $action['destination'];
    }
    else {
      $destination = isset($_REQUEST['destination']) ? $_REQUEST['destination'] : '<front>';
    }
    
    if (!empty($action['email_redirect'])) {
      $_SESSION['vkoauth']['destination'] = $destination;
      $destination = $action['email_redirect'];
    }
    drupal_goto($destination);
  }
  return $error_message;
}

/* ============================== HELPER FUNCTIONS ============================== */

/**
 * Invoke an action specified through hook_vkoauth_action_info().
 */
function vkoauth_action_invoke($action_name, $app_id, $access_token) {
  $action = vkoauth_action_load($action_name);

  // Call the specified action.
  if (isset($action['callback'])) {
    $callback = $action['callback'];
    if (function_exists($callback)) {
      return $callback($app_id, $access_token);
    }
  }
}

/**
 * Given an approval code from VK, return an access token.
 */
function vkoauth_access_token($code, $action_name, $app_id = NULL, $app_secret = NULL) {

  $app_id = isset($app_id) ? $app_id : variable_get('vkoauth_id', '');
  $app_secret = isset($app_secret) ? $app_secret : variable_get('vkoauth_secret', '');

  $query = array(
    'client_id' => $app_id,
    'client_secret' => $app_secret,
    'redirect_uri' => url('vkoauth/' . $action_name, array('absolute' => TRUE, 'query' => !empty($_GET['destination']) ? array('destination' => $_GET['destination']) : array())),
    'code' => $code,
  );

  $token_url = url(VKOAUTH_ACCESS_TOKEN_URL, array('absolute' => TRUE, 'query' => $query));
  $authentication_result = drupal_http_request($token_url);

  if ($authentication_result->code != 200) {
    watchdog('vkoauth', 'Could not acquire an access token from VK.com. We queried the following URL: <code><pre>@url</pre></code>. VK\'s servers returned an error @error: <code><pre>@return</pre></code>', array('@url' => $token_url, '@error' => $authentication_result->code, '@return' => check_plain(print_r($authentication_result->data, TRUE))));
  }
  else {
    $authentication_values = drupal_json_decode($authentication_result->data);
  }

  return isset($authentication_values['access_token']) ? $authentication_values['access_token'] : NULL;
}

/**
 * List of all VK permissions.
 */
function _vkoauth_user_permissions() {
  $permissions = array(
    'notify' => t('The user allows receiving notifications'),
    'friends' => t('Access to friends'),
    'photos' => t('Access to photos'),
    'audio' => t('Access to audio files'),
    'video' => t('Access to videos'),
    'notes' => t('Access to the user\'s notes'),
    'pages' => t('Access to wiki pages'),
    'wall' => t('Access to standard and advanced methods for working with the wall'),
    'offline' => t('Access to API at any time from an external server'),
  );

  drupal_alter('vkoauth_permissions', $permissions);
  return $permissions;
}

/**
 * Performs request for specific method.
 */
function vkoauth_method_request($id, $access_token = NULL, $parameters = array()) {

  if (isset($access_token)) {
    $parameters['access_token'] = $access_token;
  }

  $url = url(VKOAUTH_METHOD_URL . '/' . $id, array('absolute' => TRUE, 'query' => $parameters));
  $result = drupal_http_request($url);

  if (!empty($result->error)) {
    watchdog('vkoauth', 'An error received while requesting VK.com. We queried the following URL: <code><pre>@url</pre></code>. VK\'s servers returned an error @error: <code><pre>@return</pre></code>', array('@url' => $url, '@error' => $result->error, '@return' => check_plain(print_r($result->data, TRUE))));
    return drupal_set_message(t('An error received while requesting VK.com'), 'error');
  }

  return drupal_json_decode($result->data);
}

/**
 * Get basic user information.
 */
function vkoauth_users_get_request($access_token) {
  $fields = array_keys(vkoauth_user_fields_mapping());
  return vkoauth_method_request('users.get', $access_token, array('fields' => implode(',', $fields)));
}

/**
 * Given a Drupal user object, log the user in.
 */
function vkoauth_login_user($account) {
  global $user;
  
  if ($account->status) {
    $form_state['uid'] = $account->uid;
    user_login_submit(array(), $form_state);
  }
  else {
    drupal_set_message(t('The username %name has not been activated or is blocked.', array('%name' => $account->name)), 'error');
  }
  return !empty($user->uid);
}

/**
 * Connect to VK.com site
 */
function vkoauth_action_connect($app_id, $access_token) {

  global $user;
  $return = array();
  $request = vkoauth_users_get_request($access_token);
  
  $options = $request['response'][0];
  $vkid = !empty($options['uid']) ? $options['uid'] : NULL;
  $return = $options;

  $uid = vkoauth_uid_load($vkid);
  $vk_email = $vkid . '@' . VKOAUTH_EMAIL_DOMAIN;

  // If the user has logged in before, load their account and login.
  if (!$user->uid && $uid && ($account = user_load($uid))) {
    vkoauth_login_user($account);
  }

  // If the VK e-mail address matches an existing account, bind them
  // together and log in as that account.
  elseif ($account = user_load_by_mail($vk_email)) {
    vkoauth_save($account->uid, $vkid);

    // Logins will be denied if the user's account is blocked.
    if (vkoauth_login_user($account)) {
      drupal_set_message(t('You\'ve connected your account with VK.'));
    }
  }
  else {
    // If the user is already logged in, associate the two accounts.
    if ($user->uid) {
      vkoauth_save($user->uid, $vkid);
      drupal_set_message(t('You\'ve connected your account with VK.'));
    }
    // Register a new user only if allowed.
    elseif (variable_get('user_register', USER_REGISTER_VISITORS) && user_access('vkoauth create account')) {
      if (variable_get('vkoauth_require_email', TRUE)) {
        $_SESSION['vkoauth'] = $options;
        $return['email_redirect'] = 'vkoauth/email';
      }
      else {
        $options['email'] = $vk_email;
        vkoauth_create_login_user($options);
      }
    }
    // Since user's can't create new accounts on their own, show an error.
    else {
      drupal_set_message('Your VK e-mail address does not match any existing accounts. If you have an account, you must first log in before you can connect your account to VK. Creation of new accounts on this site is disabled.');
    }
  }
  return $return;
}

/**
 * Break existing connection between this site and VK.com
 */
function vkoauth_action_deauth() {

  $uid = isset($_GET['uid']) ? $_GET['uid'] : NULL;
  $redirect = isset($_GET['destination']) ? $_GET['destination'] : '<front>';
  $app_id = variable_get('vkoauth_id', '');

  if ($vkid = vkoauth_vkid_load($uid)) {
    $url = url(VKOAUTH_DEAUTHORIZE_URL, array('absolute' => TRUE, 'query' => array('client_id' => $app_id)));
    $result = drupal_http_request($url);

    if (empty($result->error)) {
      vkoauth_save($uid);
      watchdog('vkoauth', 'The account for UID @uid has been disconnected from VK by the user via VK.com.', array(
        '@uid' => $uid));

      // Allow other modules to hook into a deauth event.
      module_invoke_all('vkoauth_deauthorize', $uid, $vkid);
    }
  }
  drupal_goto($redirect);
}

/**
 * Create user account upon connection and login user onto this site
 */
function vkoauth_create_login_user($options) {

  $account = vkoauth_create_user($options);
  // Load the account fresh just to have a fully-loaded object.
  $account = user_load($account->uid);

  // If the account requires administrator approval the new account will
  // have a status of '0' and not be activated yet.
  if ($account->status == USER_REGISTER_ADMINISTRATORS_ONLY) {
    vkoauth_save($account->uid, $options['uid']);
    _user_mail_notify('register_pending_approval', $account);
    drupal_set_message(t('An account has been created for you on @sitename but an administrator needs to approve your account. In the meantime, a welcome message with further instructions has been sent to your e-mail address.', array('@sitename' => variable_get('site_name', ''))));
  }
  // Log in the user if no approval is required.
  elseif (vkoauth_login_user($account)) {
    vkoauth_save($account->uid, $options['uid']);
    drupal_set_message(t('Welcome to @sitename. Basic information has been imported from VK.com into your account. You may want to <a href="!edit">edit your account</a> to confirm the details and set a password.', array('@sitename' => variable_get('site_name', ''), '!edit' => url('user/' . $account->uid . '/edit'))));
  }
  return $account;
}

/**
 * Create user profile
 */
function vkoauth_create_user($options) {

  $status = variable_get('user_register', 0) == 1 ? 1 : 0;
  $vkid = !empty($options['uid']) ? $options['uid'] : NULL;
  $first_name = !empty($options['first_name']) ? $options['first_name'] : NULL;
  $last_name = !empty($options['last_name']) ? $options['last_name'] : NULL;
  
  $username = variable_get('vkoauth_username', '%first_name %last_name');
  $username = str_replace('%vkid', $vkid, $username);
  $username = str_replace('%first_name', $first_name, $username);
  $username = str_replace('%last_name', $last_name, $username);

  if (module_exists('transliteration') && variable_get('vkoauth_translit', 0)) {
    $username = transliteration_get($username);
  }
  
  $max_lenght = USERNAME_MAX_LENGTH - 3;
  if (drupal_strlen($username) > $max_lenght) {
    $username = substr($username, 0, $max_lenght);
  }

  $query = "SELECT uid FROM {users} WHERE name = :name";
  $uid = db_query($query, array(':name' => $username))->fetchField();
  $i = 0;
  while ($uid) {
    $i++;
    $uid = db_query($query, array(':name' => ($username . $i)))->fetchField();
  }
  if ($i > 0) {
    $username = $username . $i;
  }

  // Initialize basic properties that are unlikely to need changing.
  $edit = array(
    'name' => $username,
    'mail' => $options['email'],
    'init' => $options['email'],
    'status' => $status,
    'language' => variable_get('vkoauth_language', language_default('language')),
    'timezone' => variable_get('date_default_timezone'),
    'vkoauth_vkid' => $vkid,
  );

  $roles = variable_get('vkoauth_roles', array());
  if ($roles) {
    $edit['roles'] = $roles;
  }

  module_load_include('inc', 'vkoauth', 'includes/vkoauth.field');
  vkoauth_field_create_user($edit, $options);

  // Allow other modules to manipulate the user information before save.
  foreach (module_implements('vkoauth_user_presave') as $module) {
    $function = $module . '_vkoauth_user_presave';
    $function($edit, $options);
  }

  $account = user_save(NULL, $edit);
  // Retrieve the user's picture from VK and save it locally.
  if ($account->uid && !empty($options[VKOAUTH_AVATAR_SIZE])) {
    $picture_directory =  file_default_scheme() . '://' . variable_get('user_picture_path', 'pictures');
    if(file_prepare_directory($picture_directory, FILE_CREATE_DIRECTORY)){
      $picture_result = file_get_contents($options[VKOAUTH_AVATAR_SIZE]);
      $picture_path = file_stream_wrapper_uri_normalize($picture_directory . '/picture-' . $account->uid . '-' . REQUEST_TIME . '.jpg');
      $picture_file = file_save_data($picture_result, $picture_path, FILE_EXISTS_REPLACE);

      // Check to make sure the picture isn't too large for the site settings.
      $max_dimensions = variable_get('user_picture_dimensions', '85x85');
      file_validate_image_resolution($picture_file, $max_dimensions);

      // Update the user record.
      $picture_file->uid = $account->uid;
      $picture_file = file_save($picture_file);
      file_usage_add($picture_file, 'user', 'user', $account->uid);
      db_update('users')
        ->fields(array(
        'picture' => $picture_file->fid,
        ))
        ->condition('uid', $account->uid)
        ->execute();
    }
  }

  // Allow other modules to manipulate the user information after save.
  foreach (module_implements('vkoauth_user_save') as $module) {
    $function = $module . '_vkoauth_user_save';
    $function($account, $options);
  }
  return $account;
}

/**
 * Output a VK link.
 */
function vkoauth_action_display($action_name, $redirect = NULL) {
  // Use the default App ID if not specified.
  $app_id = variable_get('vkoauth_id', '');
  $action = vkoauth_action_load($action_name);
  $link = vkoauth_action_link_properties($action_name, $redirect, $app_id);
  $theme = isset($action['theme']) ? $action['theme'] : 'vkoauth_action__' . $action_name;
  return theme($theme, array('action' => $action, 'properties' => $link));
}

/**
 * Load specific action
 */
function vkoauth_action_load($action_name) {
  static $actions;
  // Build the list of all available actions.
  if (!isset($actions)) {
    $actions = array();
    foreach (module_implements('vkoauth_actions') as $module) {
      if ($module_actions = module_invoke($module, 'vkoauth_actions')) {
        foreach ($module_actions as $module_action_name => $module_action) {
          $module_action['name'] = $module_action_name;
          $module_action['module'] = $module;
          $module_action['file path'] = isset($module_action['file path']) ? $module_action['file path'] : drupal_get_path('module', $module);
          $module_action['properties'] = isset($module_action['properties']) ? $module_action['properties'] : array();
          $module_action['permissions'] = !empty($module_action['permissions']) ? $module_action['permissions'] : array();
          $actions[$module_action_name] = $module_action;
        }
      }
    }
    drupal_alter('vkoauth_actions', $actions);
  }

  $action = isset($actions[$action_name]) ? $actions[$action_name] : FALSE;
  // Include any necessary includes for the file.
  if ($action) {
    if (isset($action['file'])) {
      $file = './' . $action['file path'] . '/' . $action['file'];
      if (file_exists($file)) {
        include_once $file;
      }
    }
  }
  return $action;
}

/**
 * Return a set of properties suitable for use to a url() call.
 */
function vkoauth_action_link_properties($action_name, $redirect = NULL, $app_id = NULL) {

  $return = array();

  // Use the default App ID if not specified.
  $app_id = isset($app_id) ? $app_id : variable_get('vkoauth_id', '');
  $action = vkoauth_action_load($action_name);

  // Determine the required permissions for this action.
  if (!empty($action['permissions'])) {
    $permissions = $action['permissions'];
  }
  else {
    $permissions = array();
  }
  
  if (!empty($action['properties'])) {
    $properties = $action['properties'];
  }

  // Build the return query string.
  $query = array();
  if ($redirect) {
    $query['destination'] = $redirect;
  }
  elseif (!empty($_GET['destination'])) {
    $query['destination'] = $_GET['destination'];
  }
  
  $return['query']['client_id'] = $app_id;
  $return['query']['redirect_uri'] = url('vkoauth/' . $action_name, array(
    'absolute' => TRUE,
    'query' => $query,
  ));

  $return['href'] = VKOAUTH_AUTHORIZE_URL;
  
  if ($permissions) {
    $return['query']['scope'] = implode(',', $permissions);
  }  
  
  if (!empty($properties)) {
    $return = $properties;
  }

  return $return;
}

/**
 * Map user fields.
 */
function vkoauth_user_fields_mapping() {

  $fields = array(
    'uid' => array(
      'label' => t('VK ID'),
      'field_types' => array('text'),
    ),
    'first_name' => array(
      'label' => t('First name'),
      'field_types' => array('text'),
    ),
    'last_name' => array(
      'label' => t('Last name'),
      'field_types' => array('text'),
    ),
    'nickname' => array(
      'label' => t('Nickname'),
      'field_types' => array('text'),
    ),
    'screen_name' => array(
      'label' => t('Displayed name'),
      'field_types' => array('text'),
    ),
    'sex' => array(
      'label' => t('Sex'),
      'field_types' => array('text', 'list_text'),
    ),
    'bdate' => array(
      'label' => t('Birthdate'),
      'field_types' => array('text', 'date', 'datetime', 'datestamp'),
    ),
    'city' => array(
      'label' => t('City'),
      'field_types' => array('text'),
    ),
    'country' => array(
      'label' => t('Country'),
      'field_types' => array('text'),
    ),
    'timezone' => array(
      'label' => t('Timezone'),
      'field_types' => array('text'),
    ),

    'photo_medium' => array(
      'label' => t('Photo medium'),
    ),

    'contacts' => array(
      'label' => t('Contacts'),
    ),
    'education' => array(
      'label' => t('Education'),
    ),
    'home_phone' => array(
      'label' => t('Home phone'),
      'field_types' => array('text'),
    ),
    'mobile_phone' => array(
      'label' => t('Mobile phone'),
      'field_types' => array('text'),
    ),
    'connections' => array(
      'label' => t('Other social networks'),
      'field_types' => array('text', 'text_long'),
    ),
    'university' => array(
      'label' => t('University'),
      'field_types' => array('text'),
    ),
    'university_name' => array(
      'label' => t('University name'),
      'field_types' => array('text', 'text_long'),
    ),
    'faculty' => array(
      'label' => t('Faculty'),
      'field_types' => array('text'),
    ),
    'faculty_name' => array(
      'label' => t('Faculty name'),
      'field_types' => array('text'),
    ),
    'graduation' => array(
      'label' => t('Graduation'),
      'field_types' => array('text'),
    ),
    'activity' => array(
      'label' => t('Signature'),
      'field_types' => array('text', 'text_long'),
    ),
    'interests' => array(
      'label' => t('Interests'),
      'field_types' => array('text', 'text_long'),
    ),
    'about' => array(
      'label' => t('About user'),
      'field_types' => array('text_long'),
    ), 
  );

  drupal_alter('vkoauth_user_fields_mapping', $fields);
  return $fields;
}

/* ============================== DATABASE FUNCTIONS ============================== */

/**
 * Load a VK ID given a Drupal User ID.
 */
function vkoauth_vkid_load($uid = NULL) {
  $uid = $uid ? $uid : $GLOBALS['user']->uid;
  $result = db_query("SELECT vkid FROM {vkoauth_users} WHERE uid = :uid", array(':uid' => $uid));
  $vkid = $result->fetchField();
  return $vkid ? (int) $vkid : FALSE;
}

/**
 * Load a Drupal User ID given a VK ID.
 */
function vkoauth_uid_load($vkid) {
  $result = db_query("SELECT uid FROM {vkoauth_users} WHERE vkid = :vkid", array(':vkid' => $vkid));
  $uid = $result->fetchField();
  return $uid ? (int) $uid : FALSE;
}

/**
 * Save a Drupal User ID to VK ID pairing.
 */
function vkoauth_save($uid, $vkid = NULL) {
  // Delete the existing VK ID if present for this Drupal user.
  $delete_query = 'DELETE FROM {vkoauth_users} WHERE uid = :uid';

  // If setting a new VK ID for an account, also make sure no other
  // Drupal account is connected with this VK ID.
  if ($vkid) {
    $delete_query .= ' OR vkid = :vkid';
    db_query($delete_query, array(':uid' => $uid, ':vkid' => $vkid));
  }
  else {
    db_query($delete_query, array(':uid' => $uid));
  }

  if ($vkid) {
    $id = db_insert('vkoauth_users')
      ->fields(array(
        'uid' => $uid,
        'vkid' => $vkid,
      ))->execute();
  }
}
